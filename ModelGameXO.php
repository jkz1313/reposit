﻿<?php
class ModelGameXO
{
	public $playerX;
	public $playerO;
	public $key;
	public $meanings;
	
	  	
	public function checkWhoPlayer() 
	{
		$simbols=$this->meanings;
		$simbol=substr($simbols, -1);
		if ($simbol==View::X)
		{
			$sing=View::O;			
		} 
		elseif (($simbol==View::O)||(empty($simbol)))
		{
			$sing=View::X;
		}
		return $sing;
	}
	public function getPlayerName() 
    {
		$simbol=$this->checkWhoPlayer();
		if (($simbol==View::X)||(empty($simbol)))
		{
			$player=$this->playerX;
		} 
		elseif ($simbol==View::O)
		{
			$player=$this->playerO;
		}
		return $player;
	}
	public function getKey()
	{
		$key=$this->key;
		$key.=" ".$_REQUEST['rad'];
		return $key;	
	}
	public function getMeanings()
	{
		$meanings=$this->meanings;
		$meanings.=" ".$this->checkWhoPlayer();
		return $meanings;
	}
	public function getDataToStorage()
	{
		$playerX=$this->playerX;
		$playerO=$this->playerO;
		$key=$this->getKey();
		$meanings=$this->getMeanings();
		return $playerX."*".$playerO."*".$key."*".$meanings;
	}

	public function getField()
	{
		$k=$this->getKey();
		$me=$this->getMeanings();
		$key=trim($k);
		$meanings=trim($me);
		if (!empty($key)||(!empty($meanings)))
		{
			$arrkey=explode(" ", $key);
			$arrmean=explode(" ", $meanings);
			$steps=array_combine($arrkey, $arrmean);
		} 
		else 
		{ 
			$steps=array();
		}
		return $steps;
	}

	public function getMatrixforFieldandWinner()
	{
		$sizearr=View::CELL*View::CELL;
		$steps=$this->getField();
		$cleanarr=array_fill(0, $sizearr, "0");
		$fullarr=array_replace($cleanarr, $steps);
		$matrixfield=array_chunk($fullarr, (View::CELL));
		return $matrixfield;
	}
	protected function checksArrayForConvergenceHorizontalAndVertical($matrixfield)
	{
		$arrsimbolX=array_fill(0, View::CELL, View::X);
		foreach ($matrixfield as $key=>$val)
		{
			$arrcheck=array_intersect_assoc($val, $arrsimbolX);
			if ($arrcheck==$arrsimbolX)
			{
				$sing=View::X;
			}
		}
		$arrsimbolO=array_fill(0, View::CELL, View::O);
		foreach ($matrixfield as $key=>$val)
		{
			$arrcheck=array_intersect_assoc($val, $arrsimbolO);
			if ($arrcheck==$arrsimbolO)
			{
				$sing=View::O;
			}
		}
		return $sing;
	}
	protected function checkHorizontalForWinning()
	{
		$matrixfield=$this->getMatrixforFieldandWinner();
		return $this->checksArrayForConvergenceHorizontalAndVertical($matrixfield);
	}
	protected function checkVerticalForWinning()
	{
		$matrix=$this->getMatrixforFieldandWinner();
		$matrixfield=array();
		foreach ($matrix as $key => $value)
		{
			foreach($value as $k => $v)
			{
				$matrixfield[$k][$key] = $v;
			}
		}
		return $this->checksArrayForConvergenceHorizontalAndVertical($matrixfield);
	}
	
	protected function checkCollateralDiagonalForWinning()
	{
		for($i=0; $i<View::CELL; $i++)
		{
			$rez[].=($i*View::CELL)+$i;
		}
		$arrsimbolX=array_fill(0, View::CELL, View::X);
		$rezarrsimbX=array_combine($rez, $arrsimbolX);
		$steps=$this->getField();
		$arrcheck=array_intersect_assoc($steps, $rezarrsimbX);
		if ($arrcheck==$rezarrsimbX)
			{
				$sing=View::X;
			}
		$arrsimbolO=array_fill(0, View::CELL, View::O);
		$rezarrsimbO=array_combine($rez, $arrsimbolO);
		$steps=$this->getField();
		$arrcheck=array_intersect_assoc($steps, $rezarrsimbO);
		if ($arrcheck==$rezarrsimbO)
			{
				$sing=View::O;
			}
		return $sing;
	}
	protected function checkDiagonalForWinning()
	{
		for($i=0; $i<View::CELL; $i++)
		{
			$rez[].=($i+1)*View::CELL-($i+1);
		}
		$arrsimbolX=array_fill(0, View::CELL, View::X);
		$rezarrsimbX=array_combine($rez, $arrsimbolX);
		$steps=$this->getField();
		$arrcheck=array_intersect_assoc($steps, $rezarrsimbX);
		if ($arrcheck==$rezarrsimbX)
			{
				$sing=View::X;
			}
		$arrsimbolO=array_fill(0, View::CELL, View::O);
		$rezarrsimbO=array_combine($rez, $arrsimbolO);
		$steps=$this->getField();
		$arrcheck=array_intersect_assoc($steps, $rezarrsimbO);
		if ($arrcheck==$rezarrsimbO)
			{
				$sing=View::O;
			}
		return $sing;
	}
	public function getNameWinner()
	{
		$horz=$this->checkHorizontalForWinning();
		$vert=$this->checkVerticalForWinning();
		$diag=$this->checkDiagonalForWinning();
		$cldg=$this->checkCollateralDiagonalForWinning();
		//$meanings=$this->loadDateFromStorage($base);
		if (($horz==View::X)||($vert==View::X)||($diag==View::X||$cldg==View::X))
		{
			return $meanings[0];
		}
		elseif (($horz==View::O)||($vert==View::O)||($diag==View::O||$cldg==View::O))
		{
			return $this->$meanings[1];
		}
		elseif (count($this->getField())==View::CELL*View::CELL)
		{
			return false;
		}
	}
}


