﻿<?php
class View
{
	const CELL=3;
	const X="X";
	const O="O";
	private function getHead()
	{
		return	'<html><body>';
	}
	private function getTail()
	{
			return '</body></html>';
	}
	private function getBody()
	{
		return '<form action="/">
		<h2>Игроки, введите свои имена:<h2>
		<h3>Игрок X: <h3>
		<input type=text name="gamerX" value=""><br>
		<h3>Игрок O: <h3>
		<input type=text name="gamerO" value=""><br><br>
		<input type=submit name="button" value="Ввод">
		</form>';
	}
	 public function getPlayers()
	{
		if ((empty($_REQUEST['gamerX'])||(empty($_REQUEST['gamerO'])))
		||((!preg_match('|^[а-яA-Z0-9 _]+$|iu', $gamerX))
		|| (!preg_match('|^[а-яA-Z0-9 _]+$|iu', $gamerO))))
		{
			$gamerX=$_REQUEST['gamerX'];
			$gamerO=$_REQUEST['gamerO'];
			return $gamerX.'*'.$gamerO;
		} else {
			echo "Введены неверные данные в поле имен игроков!";
			return null;
		}
	}
	
	public function getHTMLStart()
	{
		return $this->getHead() . $this->getBody() . $this->getTail();
	}
	public function getFieldTable($matrix)
	{
	'<form action="/">';
	echo '<table align="center" bgcolor="#ECCEF5" border="1" cellspacing="30">';
	$i=-1;
	foreach($matrix as $key => $val)
	{
		echo "<tr>";
		foreach($val as $k=>$v)
		{
			$i++;
			if ($v==="0")
			{
				echo "<td>"."<input type=radio name='rad' value=".$i.">"."</td>";
			}
			elseif ($v==="X")
			{
				echo "<td>"."X"."</td>";
			}
			elseif ($v==="O")
			{
				echo "<td>"."O"."</td>";
			}
			"</tr>";
		}
		"</table>";
		'<form>';
	}
	return $matrix;
	}
	public function getWinnerOfDeadHead($win)
	{
		if ($win!=false)
		{
			return 'Игорок '.$win.' выйграл игру!';
		}
		else 
		{
			return 'НИЧЬЯ!:(';
		}
	}


	public function getBodyForField($player)
	{
		return
		'<h3 align="center">Игра Крестики-Нолики</h3>
		<p align="center"><input type=submit name="newgam" value="Новая игра"></p>
		<h4 align="center">Ход игрока: '.$player.'</h4>
		<p align="center"><input type=submit name="step" value="Сделать ход"></p>';
	}
	public function getHtmlField($matrix, $player)
	{
		return $this->getHead() . $this->getBodyForField($player) . $this->getFieldTable($matrix). $this->getTail();
	}
	public function getWin()
	{
		 return
		'<html><body>
		<form method="POST" action="/" >
		<h3>'.$this->getWinnerOfDeadHead().'</h3>
		<h5>Еще партейку?</h5>
		<input type=submit name="back" value="Новая игра">
		<input type=submit name="backplayers" value="Новые пользователи">
		</form>	
		</body></html>';
	}
}



		
